import React from "react";
import {  View , StyleSheet , StatusBar } from "react-native";
import Home from '../../Home'

export default function HomeScreen ({navigation}) {
    return(
        <View style={styles.container}>
        <Home/>
          <StatusBar style="auto" />
        </View>
      );
    }
    
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
      },
});
    