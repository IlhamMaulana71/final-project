import React from "react";
import { Text , View , StyleSheet,StatusBar , Image } from "react-native";
import { Icon } from "react-native-vector-icons/MaterialIcons";

export default function SettingsScreen ({navigation}) {
    return(
            <View style={styles.container}>
            <Text onPress={() => navigation.navigate('Home')}></Text>
            <Text style={styles.text1}>Tentang Saya</Text>
            <Text style={styles.text2}>Ilham Maulana</Text>
            <Text style={styles.text3}>React Native Developer</Text>
            <Text style={styles.text4}>Contact:</Text>
            <Text style={styles.text5}>083149333730</Text>
            <Text style={styles.text6}>ilhammlna340</Text>
            <Text style={styles.text7}>@gmail.com</Text> 

            <Image
              style={{ position: "absolute",
                width: 200,
                height:200,
                left: 80,
                top: 100,}}
              source={require("../../img/user.png")}/>
              <StatusBar style="auto" />
              <Image
              style={{ position: "absolute",
                width: 70,
                height:70,
                left:20,
                top: 500,}}
              source={require("../../img/wa.jpg")}/>
              <StatusBar style="auto" />
              <Image
              style={{ position: "absolute",
                width: 50,
                height:50,
                left: 30,
                top: 600,}}
              source={require("../../img/gmail.jpg")}/>
              <StatusBar style="auto" />
            </View>
            
          );
        }
        
        const styles = StyleSheet.create({
          container: {
            flex: 1,
            backgroundColor: 'white',
            alignItems: 'center',
            justifyContent: 'center',
          },
          text1:{
            position: "absolute",
            fontSize:30,
            fontWeight:"900",        
            left: 86,
            top: 40,
            color:"black",
          },
          text2:{
            position: "absolute",
            fontSize:30,
            fontWeight:"900",        
            left: 86,
            top: 310,
            color:"black",
          },
          text3:{
            position: "absolute",
            fontSize:29,
            fontWeight:"900",        
            left: 26,
            top: 350,
            color:"black",
          },
          text4:{
            position: "absolute",
            fontSize:29,
            fontWeight:"900",        
            left: 26,
            top: 450,
            color:"black",
          },
          text5:{
            position: "absolute",
            fontSize:29,
            fontWeight:"900",        
            left: 110,
            top: 510,
            color:"black",
          },
          text6:{
            position: "absolute",
            fontSize:29,
            fontWeight:"900",        
            left: 110,
            top: 590,
            color:"black",
          },
          text7:{
            position: "absolute",
            fontSize:29,
            fontWeight:"900",        
            left: 110,
            top: 620,
            color:"black",
          },
        });
        