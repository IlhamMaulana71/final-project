import React from "react";
import { Text , View , StyleSheet , StatusBar } from "react-native";
import Home from '../../HomeScreen/Home'

export default function HomeScreen ({navigation}) {
    return(
        <View style={styles.container}>
        <Text onPress={() => navigation.navigate('Home')}></Text>
        <Text style={styles.text1}>Book List</Text>
        <Home/>
          <StatusBar style="auto" />
        </View>
      );
    }
    
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
      },
      text1:{
        position: "absolute",
        fontSize:40,
        fontWeight:"900",        
        left: 86,
        top: 30,
        color:"white",
    },

    });
    