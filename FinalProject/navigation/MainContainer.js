import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Ionicons from 'react-native-vector-icons/Ionicons'

// Screens
import HomeScreen from './screens/HomeScreen'
import DetailScreen from './screens/DetailScreen'
import SettingsScreen from './screens/SettingsScreen'

// Screens names
const homeName = 'Home'
const detailName = 'Detail'
const settingsName = 'Settings'

const Tab = createBottomTabNavigator();

export default function MainContainer () {
    return(
            <Tab.Navigator
            initialRouteName={homeName}
            screenOptions={({route}) => ({
                tabBarIcon:({focused,color,size}) =>{
                    let iconName;
                    let rn = route.name;

                    if(rn === homeName) {
                        iconName = focused ? 'home' : 'home-outline'
                    } else if (rn === detailName) {
                        iconName = focused ? 'list' : 'list-outline'
                    } else if (rn === settingsName){
                        iconName = focused ? 'settings' : 'settings-outline'
                    }

                    return <Ionicons name={iconName} size={size} color={color}/>
                },
            })}>

            <Tab.Screen name={homeName} component={HomeScreen} options={{ headerShown: false  }} />
            <Tab.Screen name={detailName} component={DetailScreen} options={{ headerShown: false  }} />
            <Tab.Screen name={settingsName} component={SettingsScreen} options={{ headerShown: false  }} />


            </Tab.Navigator>
        
    );
}