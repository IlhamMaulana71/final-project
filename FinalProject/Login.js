import React, { useState } from "react";
import { Image, StyleSheet, Text, View, TextInput, Button ,  } from "react-native";

export default function Login({ navigation }) {
   const [username, setUsername] = useState("");
   const [password, setPassword] = useState("");
   const [isError, setIsError] = useState(false);

   
  const submit = () => {
    
    if(password == 12345678) {
      navigation.navigate('MainContainer', { username })
    } else {
      setIsError(true)
    }
  };
  return (
    <View style={styles.container}>
      <Text style={{fontSize:29,
        fontWeight:"bold",
        position:"absolute",
        width: 550,
        height: 550,
        left: 30,
        top: 45,
        color:"white"
        }}>Welcome to novels app</Text>
      <Image
        style={{ position: "absolute",
          width: 200,
          height:200,
          left: 80,
          top: 120,}}
        source={require("../FinalProject/img/movie.jpg")}
      />
      <View
        style={{
          backgroundColor:"white",
          padding:200,
          top:260
        }}>
      </View>
      <View>
        <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 5,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
          }}
          placeholder="Masukan Username"
          
           value={username}
           onChangeText={(value)=>setUsername(value)}
        />
        <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 5,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
          }}
          placeholder="Masukan Password"
           value={password}
           onChangeText={(value)=>setPassword(value)}
        />
        <>
        </>
        <Button onPress={submit} title="Login" color="black" />
      </View>
    </View>

    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
    justifyContent: "center",
    alignItems: "center",
  },

});
