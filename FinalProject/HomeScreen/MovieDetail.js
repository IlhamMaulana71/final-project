import React from 'react';
import { View, Text, Image } from 'react-native';
import Card from './Card';
import CardSection from './CardSection';

const prefix = 'https://image.tmdb.org/t/p/w500';

const MovieDetail = (props) => {
    const { headerContentStyles, textStyle } = styles;
    return (
        <View>
            <Card>
                <CardSection>
                    <View>
                        <Image 
                            style={{ width: 325, height: 100 ,  }}
                            source={{ uri: prefix + props.movies.backdrop_path }}
                        />
                    </View>
                </CardSection>
            </Card>
        </View>
    );
};

const styles = {
    headerContentStyles: {
        paddingLeft: 10,
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    textStyle: {
        color: 'black'
    }
};

export default MovieDetail;