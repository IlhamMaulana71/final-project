import React from 'react'
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import 'react-native-gesture-handler';
import Login from './Login';
import Home from './Home';
import HomeScreen from './navigation/screens/HomeScreen'
import MainContainer from './navigation/MainContainer'

const Stack = createStackNavigator();

export default function index() {
    return (
       <NavigationContainer>
        <Stack.Navigator initialRouteName="Login" >
          <Stack.Screen name='Login' component={Login} options={{ headerShown: false }} />
          <Stack.Screen name='Home' component={Home} options={{ headerShown: false  }} />
          <Stack.Screen name='MainContainer' component={MainContainer} options={{ headerShown: false  }} />
        </Stack.Navigator>
      </NavigationContainer>
    )
}
